var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

router.post('/', async (req, res, next) => {
    const { username, password } = req.body;
    const user = await mongoose.model('User').findOne({
      username
    });
  
    console.log("I'm here...");
  
    if (user) {
      if (user.password == password)
        res.json({ token: user._id });
      else {
        res.status(401);
        res.json({ message: "Login failed" });
      }
    } else {
      res.status(401);
      res.json({ message: "Login failed" });
    }
  });

  module.exports = router;
