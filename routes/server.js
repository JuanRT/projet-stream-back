const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const { Socket } = require('dgram');
const app = express();
const server = http.createServer(app);
const io = socketio(server);



app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', socket => {
    console.log('New WS Connection...');
    
    socket.emit('message' , 'Welcome to the chat');


    socket.broadcast.emit('message', 'a user joined the chat');

    socket.on('disconnect' () => {
        io.emit('message', 'a user left the chat');
    });

    socket.on('chatMessage', msg => {
        io.emit('message', msg);
    });
});

const Port = 3000 || process.env.PORT;

server.listen(PORT, () => console.log('Server running on port ${PORT} '));


// page.HTML /

{/* <script src="/socket.io/socket.io.js"> </script> */}
